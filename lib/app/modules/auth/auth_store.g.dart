// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AuthStore on _AuthStoreBase, Store {
  final _$isLoadingAtom = Atom(name: '_AuthStoreBase.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$showSingInAtom = Atom(name: '_AuthStoreBase.showSingIn');

  @override
  bool get showSingIn {
    _$showSingInAtom.reportRead();
    return super.showSingIn;
  }

  @override
  set showSingIn(bool value) {
    _$showSingInAtom.reportWrite(value, super.showSingIn, () {
      super.showSingIn = value;
    });
  }

  final _$animationControllerAtom =
      Atom(name: '_AuthStoreBase.animationController');

  @override
  AnimationController get animationController {
    _$animationControllerAtom.reportRead();
    return super.animationController;
  }

  @override
  set animationController(AnimationController value) {
    _$animationControllerAtom.reportWrite(value, super.animationController, () {
      super.animationController = value;
    });
  }

  final _$singInAsyncAction = AsyncAction('_AuthStoreBase.singIn');

  @override
  Future<dynamic> singIn() {
    return _$singInAsyncAction.run(() => super.singIn());
  }

  final _$_AuthStoreBaseActionController =
      ActionController(name: '_AuthStoreBase');

  @override
  void alterShowSing({@required bool sinInShow}) {
    final _$actionInfo = _$_AuthStoreBaseActionController.startAction(
        name: '_AuthStoreBase.alterShowSing');
    try {
      return super.alterShowSing(sinInShow: sinInShow);
    } finally {
      _$_AuthStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isLoading: ${isLoading},
showSingIn: ${showSingIn},
animationController: ${animationController}
    ''';
  }
}

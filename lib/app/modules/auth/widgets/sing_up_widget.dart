import 'package:animation_login/app/config/input_decoration.dart';
import 'package:animation_login/app/config/pallet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../auth_store.dart';
import 'sing_bar.dart';

class SingUpWidget extends StatefulWidget {
   @override
  _SingUpWidgetState createState() => _SingUpWidgetState();
}

class _SingUpWidgetState extends ModularState<SingUpWidget,AuthStore> {
  @override
  void initState() {
    store.emailController= TextEditingController();
    store.passwordController= TextEditingController();
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(30),
        child: Column(
          children: [
            Expanded(
              flex: 3,
              child: Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 60),
                child: Text("Create\nAccount",style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,

                ),textAlign: TextAlign.center,),
              ),
            ),
            Expanded(
              flex: 4,
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: ListView(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                      controller: store.emailController,
                      decoration: inputLigth("EMAIL"),
                  ),
                    ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                    controller: store.passwordController,
                    decoration: inputLigth( "PASSWORD"),
                    obscureText: true,
                    ),
                  ),
                  SingInBar(
                    label: "Sing up",
                    onPress:store.singIn,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: TextButton(
                      onPressed:()=> store.alterShowSing(sinInShow: true),
                      child: Text('Sing in',style: TextStyle(
                          color: Pallete.white,
                          fontSize: 16,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:animation_login/app/config/pallet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../auth_store.dart';

class SingInBar extends StatefulWidget {
  final String label;
  final VoidCallback onPress;

  const SingInBar({Key key, this.label, this.onPress}) : super(key: key);

  @override
  _SingInBarState createState() => _SingInBarState();
}

class _SingInBarState extends ModularState<SingInBar,AuthStore> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:const EdgeInsets.symmetric(vertical: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(widget.label,
            style: TextStyle(
              fontWeight: FontWeight.w800,
              color: store.showSingIn
                ?Pallete.darkBlue
                :Pallete.white,
              fontSize: 24
           ),
          ),
          Observer(
            builder: (_) => Expanded(
              child: Center(
                child: _LoadingIndicator(isLoading:store.isLoading,),
              ),
            ),
          ),
          _RoundContinueButton(
            onPress:widget.onPress,
          )
        ],
      ),
    );
  }
}

class _LoadingIndicator extends StatelessWidget {

  final bool isLoading;

  const _LoadingIndicator({Key key, this.isLoading}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxWidth: 100),
      child: Visibility(
        visible: isLoading,
        child: const LinearProgressIndicator(
          backgroundColor: Pallete.darkBlue,
        ),
      ),
    );
  }
}



class _RoundContinueButton extends StatelessWidget {

  final VoidCallback onPress;

  const _RoundContinueButton({Key key, this.onPress}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onPress,
      elevation: 1,
      fillColor: Pallete.darkBlue,
      splashColor: Pallete.darkOrange,
      padding:const EdgeInsets.all(12),
      shape:const CircleBorder(),
      child: const Icon(
        FontAwesomeIcons.longArrowAltRight,
        color: Pallete.white,
        size: 24,
      ),

    );
  }
}


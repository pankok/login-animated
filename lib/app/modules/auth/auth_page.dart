import 'package:animation_login/app/modules/auth/widgets/sing_up_widget.dart';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'auth_store.dart';
import 'widgets/background_painter.dart';
import 'widgets/sing_in_widget.dart';

class AuthPage extends StatefulWidget {
  final String title;
  const AuthPage({Key key, this.title = "AuthPage"}) : super(key: key);
  @override
  AuthPageState createState() => AuthPageState();
}
class AuthPageState extends ModularState<AuthPage,AuthStore>
    with SingleTickerProviderStateMixin {
  

  @override
  void initState() {
    store.animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 2));
    super.initState();
  }

  @override
  dispose() {
    store.animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SizedBox.expand(
            child: CustomPaint(
              painter: BackgroundPainter(
                animation: store.animationController.view,
              ),
            ),
          ),
         
          Observer(
            builder: (_) {
              return PageTransitionSwitcher(
                reverse: !store.showSingIn,
                duration: Duration(seconds: 2),
                transitionBuilder: (child,animation,secondaryAnimation){
                  return SharedAxisTransition(
                    animation: animation,
                    secondaryAnimation: secondaryAnimation,
                    transitionType:SharedAxisTransitionType.vertical,
                    fillColor: Colors.transparent,
                    child: child,
                  );
                  
                },
                child: store.showSingIn
                  ?SingInWidget(
                    )
                  :SingUpWidget(
                    ),
              );
            }
          ),
        ],
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

part 'auth_store.g.dart';

class AuthStore = _AuthStoreBase with _$AuthStore;
abstract class _AuthStoreBase with Store {

  TextEditingController emailController;
  TextEditingController passwordController;

  @observable
  bool isLoading=false;

  @observable
  bool showSingIn=true;

  @observable
  AnimationController animationController;

  @action
  Future singIn() async{
    isLoading=true;
    await Future.delayed(Duration(seconds: 3),(){
      print("user log");
    });
    isLoading=true;
  }

  @action
  void alterShowSing({@required bool sinInShow}){
    emailController.dispose();
    passwordController.dispose();
    if(!sinInShow){
      print("user forw");
      animationController.forward();
    }else{
      print("user rever");
      animationController.reverse();
    }
    showSingIn=sinInShow;
  }
}
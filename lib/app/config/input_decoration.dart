import 'package:flutter/material.dart';
import 'pallet.dart';


InputDecoration inputDark(String hintText){
  return InputDecoration(
    contentPadding: EdgeInsets.symmetric(vertical: 15),
    hintStyle: const TextStyle(color: Colors.black87, fontSize: 15),
    hintText: hintText,
    focusedBorder:  const UnderlineInputBorder(borderSide: BorderSide(color:Pallete.darkBlue,width: 2),),
    enabledBorder:  const UnderlineInputBorder(borderSide: BorderSide(color:Pallete.darkBlue),),
    errorBorder:  const UnderlineInputBorder(borderSide: BorderSide(color:Pallete.darkOrange),),
    focusedErrorBorder:  const UnderlineInputBorder(borderSide: BorderSide(color:Pallete.darkOrange,width: 2),),
    errorStyle: const TextStyle(color: Pallete.lightBlue),
  );
}
InputDecoration inputLigth(String hintText){
  return InputDecoration(
    contentPadding: EdgeInsets.symmetric(vertical: 15),
    hintStyle: const TextStyle(color: Colors.white, fontSize: 15),
    hintText: hintText,
    focusedBorder:  const UnderlineInputBorder(borderSide: BorderSide(color:Pallete.white,width: 2),),
    enabledBorder:  const UnderlineInputBorder(borderSide: BorderSide(color:Pallete.white),),
    errorBorder:  const UnderlineInputBorder(borderSide: BorderSide(color:Pallete.darkOrange),),
    focusedErrorBorder:  const UnderlineInputBorder(borderSide: BorderSide(color:Pallete.darkOrange,width: 2),),
    errorStyle: const TextStyle(color: Pallete.lightBlue),
  );
}